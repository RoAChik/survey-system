/*
* INPUT PARAMETERS:
* 1 - Params values for update
*/
 UPDATE params AS param SET
	value = t.value
 FROM (values
		%s
	) AS t(attr_id, object_id, value)
 WHERE 	t.attr_id = param.attr_id AND
		t.object_id = param.object_id