/*
* INPUT PARAMETERS:
* 1 - IS_REQUIRED_ATTRIBUTE
* 2 - ORDER_ATTRIBUTE
* 3 - questionnairesId
* 4 - OBJECT_TYPE_CURATOR_QUESTIONNAIRE
* 5 - IS_OBSOLETE_ATTRIBUTE
* 5 - questionnairesId
* 6 - OBJECT_TYPE_FORM
*/
 SELECT
   obj.*, answers.value as field_value,
   COALESCE(is_req_param.value, 'false') as is_required,
   cast(COALESCE(order_param.value, '-1') as int) AS fields_order,
   cast(COALESCE(is_obs_param.value, 'false') as boolean) as is_obsolete, null as pseudonym
 FROM objects obj
	LEFT JOIN params is_req_param
      ON obj.object_id = is_req_param.object_id
      AND is_req_param.attr_id = ?
	LEFT JOIN params order_param
      ON obj.object_id = order_param.object_id
      AND order_param.attr_id = ?
	LEFT JOIN objects questionnaire
      ON questionnaire.object_id = ?
	  AND questionnaire.object_type_id = ?
	LEFT JOIN params answers
      ON answers.object_id = questionnaire.object_id
      AND answers.attr_id = obj.object_id
	LEFT JOIN params is_obs_param
	  ON obj.object_id = is_obs_param.object_id
	  AND is_obs_param.attr_id = ?
 WHERE
   obj.object_id IN
   (
      select object_id from objects t where object_id in (select object_id from transition where parent_id in (select parent_id from objects where object_id = ?)) and t.object_type_id != ?
   )
 ORDER BY fields_order