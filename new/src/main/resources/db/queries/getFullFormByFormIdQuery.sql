/*
* INPUT PARAMETERS:
* 1 - Constants.IS_REQUIRED_ATTRIBUTE
* 2 - Constants.ORDER_ATTRIBUTE
* 3 - IS_OBSOLETE_ATTRIBUTE
* 4 - objectId
* 5 - Constants.OBJECT_TYPE_CURATOR_FORM
* 6 - objectId
*/

 SELECT obj.*, cast(COALESCE(is_req_param.value, 'false') as boolean) as is_required,
		cast(COALESCE(order_param.value, '-1') as int) AS fields_order, 
        cast(COALESCE(is_obs_param.value, 'false') as boolean) as is_obsolete,
        NULL as field_value ,NULL as pseudonym
 FROM objects obj
	LEFT JOIN params is_req_param ON 
		obj.object_id = is_req_param.object_id 
		AND is_req_param.attr_id = ?
	LEFT JOIN params order_param ON 
		obj.object_id = order_param.object_id AND 
		order_param.attr_id = ?
	LEFT JOIN params is_obs_param ON
		obj.object_id = is_obs_param.object_id AND 
		is_obs_param.attr_id = ? 
    LEFT JOIN objects cur_form ON
    	cur_form.parent_id = ? AND
        cur_form.object_type_id = ?
 WHERE obj.object_id IN
		(SELECT object_id 
		 FROM transition 
		 WHERE 	parent_id IN (?, cur_form.object_id)
		)
 ORDER BY fields_order