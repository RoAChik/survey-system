/*
* INPUT PARAMETERS:
* 1 - formId
* 2 - OBJECT_TYPE_CURATOR_FORM
* ----
* 1th - filterArgs
*/
 SELECT qvst.parent_id AS stud_questionnaire,
		param.object_id AS questionnaire_id,  
		param.attr_id AS field_id, 
		attrs.name AS field_name, 
		param.value AS field_value  
 FROM params param
    LEFT JOIN objects attrs ON (attrs.object_id = param.attr_id)  
    JOIN objects qvst ON (qvst.object_id = param.object_id)
    %s
 WHERE param.attr_id IN
		(
			 SELECT o.object_id 
			 FROM objects o, objects cur  
			 WHERE cur.parent_id = ? AND  
				   cur.object_type_id = ? AND  
				   o.parent_id = cur.object_id AND  
				   o.object_type_id BETWEEN 100 AND 200
		)  
 ORDER BY questionnaire_id, field_id