CREATE TABLE "objects" (
	"object_id" bigint NOT NULL,
	"parent_id" bigint NOT NULL,
	"object_type_id" bigint NOT NULL,
	"name" TEXT,
	"description" TEXT,
	CONSTRAINT objects_pk PRIMARY KEY ("object_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "object_types" (
	"object_type_id" bigint NOT NULL,
	"name" TEXT,
	CONSTRAINT object_types_pk PRIMARY KEY ("object_type_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "params" (
	"attr_id" bigint NOT NULL,
	"object_id" bigint NOT NULL,
	"value" TEXT,
	UNIQUE ("attr_id", "object_id", "value")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "transition" (
	"object_id" bigint NOT NULL,
	"parent_id" bigint NOT NULL,
	"level" int NOT NULL,
	UNIQUE ("object_id", "parent_id", "level")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "ids" (
	"current_id" bigint NOT NULL,
	CONSTRAINT ids_pk PRIMARY KEY ("current_id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "grants"
(
    role_id bigint NOT NULL,
    object_id bigint NOT NULL,
    grants bigint NOT NULL,
    CONSTRAINT grants_role_id_object_id_pk PRIMARY KEY (role_id, object_id)
);

ALTER TABLE "objects" ADD CONSTRAINT "objects_fk0" FOREIGN KEY ("parent_id") REFERENCES "objects"("object_id");
ALTER TABLE "objects" ADD CONSTRAINT "objects_fk1" FOREIGN KEY ("object_type_id") REFERENCES "object_types"("object_type_id");


ALTER TABLE "params" ADD CONSTRAINT "params_fk0" FOREIGN KEY ("attr_id") REFERENCES "objects"("object_id");
ALTER TABLE "params" ADD CONSTRAINT "params_fk1" FOREIGN KEY ("object_id") REFERENCES "objects"("object_id") ON DELETE CASCADE;

ALTER TABLE "transition" ADD CONSTRAINT "transition_fk0" FOREIGN KEY ("object_id") REFERENCES "objects"("object_id") ON DELETE CASCADE;
ALTER TABLE "transition" ADD CONSTRAINT "transition_fk1" FOREIGN KEY ("parent_id") REFERENCES "objects"("object_id");


