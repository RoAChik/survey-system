app.service('FormService', function FormService($http) {

    return {
        fields:[
            {
                name : 'text',
                value : 'Short answer'
            },
            {
                name : 'textarea',
                value : 'Paragraph'
            },
            {
                name : 'date',
                value : 'Date'
            },
            {
                name : 'radio',
                value : 'Radiobuttons'
            },
            {
                name : 'checkbox',
                value : 'Checkboxes'
            },
            {
                name : 'list',
                value : 'Drop-down list'
            },
            {
                name : 'email',
                value : 'Email'
            },
            {
                name : 'phone_number',
                value : 'Phone number'
            },
            {
                name : 'bounded_number',
                value : 'Number'
            },
            {
                name : 'year_of_birth',
                value : 'Year of birth'
            }
        ]
    };
});