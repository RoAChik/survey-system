app.controller('ConfirmDeleteController',
    function ConfirmDeleteController($scope, $http, $routeParams, $uibModalInstance, $location, Page) {

        $scope.deleteQue = function() {
            $http.get(Page.address()+'/api/private/deleteQue/?queId=' + $routeParams.deleteQueId).success(function () {
                $uibModalInstance.close();
                location.reload();
            });
            //alert("Que is deleted");
        };

        $scope.closeConfirmPage = function() {
            $uibModalInstance.close();
        };
    }
);