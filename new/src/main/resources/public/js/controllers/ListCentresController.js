app.controller('ListCentresController',
    function ListCentresController($scope, $http, $timeout, Page,$uibModal) {
        $scope.grants = {};
        $scope.data = {}
        $http.get(Page.address()+'/api/private/getCentres/').then(function (response) {
            $scope.data = response.data;
        },function (response){
            if(response.status == 403) {
                $window.location.reload();
            }
        });
        /*$http.get(Page.address()+'/api/private/getGrants').success(function (data) {
            $scope.grants = data;
            $scope.hasAdminGrants = $scope.grants.hasAdminGrants;
            $scope.hasAdminCentreGrants = $scope.grants.hasAdminCentreGrants;
        });*/

        $scope.add = function() {
            $uibModal.open({
                templateUrl: "/views/templates/modal/add.html",
                controller: function($scope,$uibModalInstance, $window){

                    $scope.centre = {};

                    $scope.getFormID = function(object){
                        $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                            object.objectId = data;
                        });
                    };
                    $scope.getFormID($scope.centre);


                    $scope.submit = function(){
                        $scope.centre.name = $scope.name;
                        $http.post(Page.address() + '/api/private/addCentre', $scope.centre).then(function (response) {
                            $uibModalInstance.close();
                            $window.location.reload();
                        }, function (response) {
                        });
                    }

                }
            });
        };

        $scope.edit = function(id) {
            $uibModal.open({
                templateUrl: "/views/templates/modal/edit.html",
                controller: function($scope,$uibModalInstance, $window){

                    $scope.centre = {};

                    $scope.getData = function(){
                        $http.get(Page.address()+'/api/private/getCentre?id='+id).success(function (data) {
                            $scope.centre.objectId = data.objectId;
                            $scope.name = data.name;
                            $scope.centre.name = $scope.name;
                        });
                    };

                    $scope.getData();


                   $scope.submit = function(){
                        $scope.centre.name = $scope.name;
                        $http.post(Page.address() + '/api/private/editCentre', $scope.centre).then(function (response) {
                            $uibModalInstance.close();
                            $window.location.reload();
                        }, function (response) {
                        });
                    }

                }
            });
        };

    }
);
app.controller('ListUsersController',
    function ListUsersController($scope, $http, $routeParams, Page,$uibModal) {
        $scope.grants = {};
        $scope.data = {}
        $http.get(Page.address()+'/api/private/getUsers/?id='+$routeParams.id).then(function (response) {
            $scope.data = response.data;
        },function (response){
            if(response.status == 403) {
                $window.location.reload();
            }
        });

        $scope.add = function() {
            $uibModal.open({
                templateUrl: "/views/templates/modal/add.html",
                controller: function($scope,$uibModalInstance, $window,$routeParams){

                    $scope.object = {};
                    $scope.object.parentId = $routeParams.id;

                    $scope.getFormID = function(object){
                        $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                            object.objectId = data;
                        });
                    };
                    $scope.getFormID($scope.object);


                    $scope.submit = function(){
                        $scope.object.name = $scope.name;
                        $http.post(Page.address() + '/api/private/addUser', $scope.object).then(function (response) {
                            if(response.data.params.error != true){
                                $uibModalInstance.close();
                                $window.location.reload();
                            } else {
                                alert(response.data.params.errorMessage);
                            }

                        }, function (response) {
                        });
                    }

                }
            });
        };

        $scope.edit = function(id) {
            $uibModal.open({
                templateUrl: "/views/templates/modal/edit_user.html",
                controller: function($scope,$uibModalInstance, $window){

                    $scope.object = {};
                    $scope.selectedValues = [];
                    $scope.getData = function(){
                        $http.get(Page.address()+'/api/private/getUser?id='+id).success(function (data) {

                            $scope.object = data.map.user;
                            console.log($scope.object);
                            $scope.rolesList = data.map.rolesList.myArrayList;
                            for(var i = 0; i< $scope.rolesList.length;i++){
                                $scope.rolesList[i].map.is = false;
                                for(var j = 0; j < $scope.object.roles.length;j++){
                                    if($scope.rolesList[i].map.objectId == $scope.object.roles[j].objectId){
                                        $scope.selectedValues.push($scope.rolesList[i]);
                                    }
                                }
                            }
                            $scope.stateChangedSingle();

                        });
                    };
                    $scope.getData();

                    $scope.stateChangedSingle = function(){
                       console.log($scope.selectedValues);
                        $scope.object.roles = [];
                        for(var i = 0; i< $scope.selectedValues.length;i++){
                            $scope.object.roles.push($scope.selectedValues[i].map);
                        }
                        //$scope.object.roles = $scope.selectedValues;
                    };

                    $scope.submit = function(){
                        console.log($scope.object);
                        //$scope.object.name = $scope.name;
                        $http.post(Page.address() + '/api/private/editUser', $scope.object).then(function (response) {
                            if(response.data.params.error != true){
                                $uibModalInstance.close();
                                $window.location.reload();
                            } else {
                                alert(response.data.params.errorMessage);
                            }
                        }, function (response) {
                        });
                    }

                }
            });
        };

    }
);

app.controller('ListRolesController',
    function ListRolesController($scope, $http, $routeParams, Page,$uibModal) {
        $scope.grants = {};
        $scope.data = {}
        $http.get(Page.address()+'/api/private/getRoles/?id='+$routeParams.id).then(function (response) {
            $scope.data = response.data;
        },function (response){
            if(response.status == 403) {
                $window.location.reload();
            }
        });

        $scope.add = function() {
            $uibModal.open({
                templateUrl: "/views/templates/modal/add.html",
                controller: function($scope,$uibModalInstance, $window,$routeParams){

                    $scope.object = {};
                    $scope.object.parentId = $routeParams.id;

                    $scope.getFormID = function(object){
                        $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                            object.objectId = data;
                        });
                    };
                    $scope.getFormID($scope.object);


                    $scope.submit = function(){
                        $scope.object.name = $scope.name;
                        $http.post(Page.address() + '/api/private/addRole', $scope.object).then(function (response) {
                            $uibModalInstance.close();
                            $window.location.reload();
                        }, function (response) {
                        });
                    }

                }
            });
        };

        $scope.edit = function(id) {
            $uibModal.open({
                templateUrl: "/views/templates/modal/edit.html",
                controller: function($scope,$uibModalInstance, $window){

                    $scope.object = {};

                    $scope.getData = function(){
                        $http.get(Page.address()+'/api/private/getRole?id='+id).success(function (data) {
                            $scope.object.objectId = data.objectId;
                            $scope.name = data.name;
                            $scope.object.name = $scope.name;
                        });
                    };
                    $scope.getData();


                    $scope.submit = function(){
                        $scope.object.name = $scope.name;
                        $http.post(Page.address() + '/api/private/editRole', $scope.object).then(function (response) {
                            $uibModalInstance.close();
                            $window.location.reload();
                        }, function (response) {
                        });
                    }

                }
            });
        };

    }
);