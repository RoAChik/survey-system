app.controller('ModalController',
    function ModalController($scope, $http, $routeParams, $uibModalInstance, $location, Page) {
        $scope.data = {};
        $scope.hasAdminGrants = {};
        $scope.form = {};
        $http.get(Page.address()+'/api/private/getFormForModal/?id='+$routeParams.idQuestionnair).success(function (data) {
            $scope.form = data;
            console.log(data);
        });

        $scope.submit = function() {
                $scope.form.queId = $routeParams.idQuestionnair;
                $http.post(Page.address() + '/api/public/submitQuestionnaire', $scope.form).then(function (response) {
                    $uibModalInstance.close();
                    $location.path("/questionnaires/" + $routeParams.id);
                }, function (response) {
                });
        };

        $scope.isValid = function(){
            return ($scope.form.$valid)
        }

    }
);