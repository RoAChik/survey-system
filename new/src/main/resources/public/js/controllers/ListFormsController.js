app.controller('ListFormsController',
    function ListFormsController($scope, $http, $timeout, Page,$routeParams) {
        $scope.grants = {};
        $scope.data = {}
        $scope.hasCuratorGrants = {};
        $scope.hasHRGrants = {};
        $scope.centreId = $routeParams.id;
        $http.get(Page.address()+'/api/private/getForms/?id='+$routeParams.id).then(function (response) {
            $scope.data = response.data;
            //console.log(response.data);
        },function (response){
            console.log(response);
            if(response.status == 403) {
                $window.location.reload();
            }
        });


    }
);