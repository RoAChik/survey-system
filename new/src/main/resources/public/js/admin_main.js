var app = angular.module('educ', ["ui.bootstrap","ngAnimate","ngRoute","ngCookies"])
    .config(function ($routeProvider) {
        $routeProvider.when('/',
            {
                templateUrl: '/views/templates/centres_list.html',
                controller: 'ListCentresController'
            }).when('/centres',
            {
                templateUrl: '/views/templates/centres_list.html',
                controller: 'ListCentresController'
            }).when('/users/:id',
            {
                templateUrl: '/views/templates/users_list.html',
                controller: 'ListUsersController'
            }).when('/roles/:id/',
            {
                templateUrl: '/views/templates/roles_list.html',
                controller: 'ListRolesController'
            }).when('/builder/create/:id/',
            {
                templateUrl: '/views/templates/create.html',
                controller: 'CreateController'
            }).when('/questionnaires/:id/',
            {
                templateUrl: '/views/templates/list.html',
                controller: 'ListQuestionnairesController'
            }).when('/builder/list/:id/',
            {
                templateUrl: '/views/templates/forms_list.html',
                controller: 'ListFormsController'
            }).when('/builder/edit/:id/',
            {
                templateUrl: '/views/templates/edit.html',
                controller: 'EditController'
            }).when('/error404',
            {
                templateUrl: '/views/templates/404.html'
            })
            .otherwise({
                templateUrl: '/views/templates/404.html'
            });
    }).factory('Page', function($location){
        var title = 'EDUC';
        var address = "http://"+$location.host()+":"+$location.port();
        return {
            title: function() { return title; },
            address: function() { return address; },
            setTitle: function(newTitle) { title = newTitle; }
        };
    }).controller('MainController',
        function MainController($scope,$http, Page) {
            $scope.Page = Page;
            $http.get(Page.address()+'/api/private/getGrants').success(function (data) {
                $scope.grants = data;
                $scope.hasAdminGrants = $scope.grants.hasAdminGrants;
                $scope.hasAdminCentreGrants = $scope.grants.hasAdminCentreGrants;
            });
        });

