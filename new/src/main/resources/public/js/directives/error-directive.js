app.directive('errorDirective', function ($http) {
    return {

        templateUrl: './views/directives/404.html',
        restrict: 'E',
        scope: {
            form:'=',
            addField:'='
        }
    };
});