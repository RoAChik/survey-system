<!DOCTYPE html>
<html lang="en" ng-app="educ" ng-controller="MainController">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <meta http-equiv="CACHE-CONTROL" content="NO-STORE">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title ng-bind="Page.title()"></title>
    <link rel="stylesheet" href="/css/bootstrap.css"/>
    <link rel="stylesheet" href="/css/form.css"/>
</head>
<body>

<div class="netcracker-bar">
    <div class="container">
        <div class="subheader">
            <div class="pull-left" style="display:inline-block">
                <img src="/image/logo.png"/>
            </div>
        </div>
    </div>
</div>

<div class="nectracker-background">

</div>

<div class="container" style="margin-top: -130px">
<div ng-view></div>
</div>

<script type="text/javascript" src="/js/libs/angular.js"></script>
<script type="text/javascript" src="/js/libs/angular-route.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-cookies.js"></script>
<script type="text/javascript" src="/js/libs/angular-animate.js"></script>
<script type="text/javascript" src="/js/libs/ui-bootstrap-tpls-2.2.0.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/services/form-service.js"></script>
<script type="text/javascript" src="/js/controllers/formController.js"></script>
<script type="text/javascript" src="/js/directives/form-directive.js"></script>
<script type="text/javascript" src="/js/directives/error-directive.js"></script>
<script type="text/javascript" src="/js/directives/field-directive.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-91880373-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>