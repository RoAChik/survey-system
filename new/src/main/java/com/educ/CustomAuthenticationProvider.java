package com.educ;

import com.educ.model.User;
import com.educ.model.UserRole;
import com.educ.service.UserModel;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by anbu0216 on 13.05.2017.
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private static UserModel userModel = new UserModel();

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            ArrayList<SurveyGrantedAuthority> authorities = new ArrayList<>();
            User user = userModel.getObjectByName(authentication.getName());
            String password = DigestUtils.md5Hex(authentication.getCredentials().toString()).toUpperCase();
            if(password.length() > 0 && user.getPassword().equals(password)){
                for(UserRole role : user.getRoles()){
                    authorities.add(new SurveyGrantedAuthority(role.getObjectId()));
                }
                return new UsernamePasswordAuthenticationToken(
                        user, user.getPassword(), authorities);
            }
            else return null;

        }
        catch (Exception e){return null;}
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}
