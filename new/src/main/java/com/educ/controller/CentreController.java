package com.educ.controller;

import com.educ.model.Centre;
import com.educ.service.CentreModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by anbu0216 on 13.04.2017.
 */
@RestController
public class CentreController {

    CentreModel model = new CentreModel();

    @RequestMapping(value = "/api/private/getCentres",method = RequestMethod.GET)
    public Map<Long, Map<String,String>> getList() throws SQLException {
        return model.getObjects();
    }

    @RequestMapping(value = "/api/private/getCentre",method = RequestMethod.GET)
        public Centre getCentre(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        return model.getObject(id);
    }

    @RequestMapping(value = "/api/private/addCentre", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void addCentre(@RequestBody Centre centre) throws SQLException {
        centre.setParentId(centre.getObjectId());
        model.addObject(centre);
    }

    @RequestMapping(value = "/api/private/editCentre", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void editCentre(@RequestBody Centre centre) throws SQLException {
        model.editObject(centre);
    }

}
