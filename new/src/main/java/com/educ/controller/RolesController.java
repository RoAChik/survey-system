package com.educ.controller;

import com.educ.model.UserRole;
import com.educ.service.RolesModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by anbu0216 on 13.04.2017.
 */
@RestController
public class RolesController {

    RolesModel model = new RolesModel();

    @RequestMapping(value = "/api/private/getRoles",method = RequestMethod.GET)
    public Map<Long, Map<String,String>> getList(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        return model.getObjectsByParent(id);
    }

    @RequestMapping(value = "/api/private/addRole", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void addRole(@RequestBody UserRole role) throws SQLException {
        model.addObject(role);
    }

    @RequestMapping(value = "/api/private/getRole",method = RequestMethod.GET)
    public UserRole getRole(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        return (UserRole)model.getObject(id);
    }

    @RequestMapping(value = "/api/private/editRole", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void editRole(@RequestBody UserRole role) throws SQLException {
        model.editObject(role);
    }
}
