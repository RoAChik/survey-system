package com.educ.controller;

import com.educ.model.User;
import com.educ.model.UserRole;
import com.educ.model.common.Response;
import com.educ.service.*;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by anbu0216 on 13.04.2017.
 */
@RestController
public class UsersController {

    UserModel model = new UserModel();
    RolesModel rolesModel = new RolesModel();
    GrantsModel grantsModel = new GrantsModel();

    @RequestMapping(value = "/api/private/getUsers",method = RequestMethod.GET)
    public Map<Long, Map<String,String>> getList(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        return model.getObjectsByParent(id);
    }

    @RequestMapping(value = "/api/private/addUser", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public Response addUser(@RequestBody User user) throws SQLException {
        Response response = new Response();
        model.addObject(user,response);
        return response;
    }

    @RequestMapping(value = "/api/private/getUser",method = RequestMethod.GET)
    public String getUser(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        User user = model.getObject(id);
        Map<Long, Map<String,String>> allRoles = rolesModel.getObjectsByParent(user.getParentId());

        ArrayList<Map<String,String>> rList = new ArrayList<>();
        for (Map<String, String> stringStringMap : allRoles.values()) {
            rList.add(stringStringMap);
        }
        CurrentSecurityMap currentSecurityMap = new CurrentSecurityMap();
        if(currentSecurityMap.hasAdminGrants()){
            rList.add(new HashMap(){{
                put("name","Administrator");
                put("objectId", Long.toString(Constants.ADMIN_ROLE));
            }});
            rList.add(new HashMap(){{
                put("name","Administrator Education Centre");
                put("objectId", Long.toString(Constants.ADMIN_CENTRE_ROLE));
            }});

        }
        if(currentSecurityMap.hasAdminCentreGrants()){
            Map<String, String> adminRoleMap = new HashMap<>();
            adminRoleMap.put("name","Administrator Education Centre");
            adminRoleMap.put("objectId", Long.toString(Constants.ADMIN_CENTRE_ROLE));
            rList.add(adminRoleMap);
        }
        JSONObject json = new JSONObject();
        json.put("user",user);
        json.put("rolesList",rList);
        Gson gson = new Gson();
        return gson.toJson(json);
    }

    @RequestMapping(value = "/api/private/editUser", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public Response editUser(@RequestBody User user) throws SQLException {
        Response response = new Response();
        model.editObject(user,response);
        return response;
    }

    @RequestMapping(value = "/api/private/getFieldGrants", method = RequestMethod.GET)
    public String getFieldGrants(@RequestParam(value="id", defaultValue="1") String id, @RequestParam(value="form", defaultValue="1") String form) throws SQLException {
        //System.out.println(user.getObjectId());
        JSONArray array = new JSONArray();
        GrantsModel grantsModel = new GrantsModel();
        HashMap<UserRole,Integer> grants =  grantsModel.getGrantsForField(Long.valueOf(id),Long.valueOf(form));
        for (Map.Entry<UserRole, Integer> userRoleIntegerEntry : grants.entrySet()) {
            JSONObject object = new JSONObject();
            object.put("role",userRoleIntegerEntry.getKey());
            System.out.println("fff "+userRoleIntegerEntry.getValue());
            object.put("readGrant",grantsModel.checkClear(userRoleIntegerEntry.getValue(),GrantsModel.READ));
            object.put("writeGrant",grantsModel.checkClear(userRoleIntegerEntry.getValue(),GrantsModel.WRITE));
            array.put(object);
        }
        Gson gson = new Gson();
        return gson.toJson(array);
    }
}
