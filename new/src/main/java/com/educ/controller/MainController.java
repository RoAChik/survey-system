package com.educ.controller;

import com.educ.service.JDBCUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Optional;

@org.springframework.stereotype.Controller

public class MainController {

    @RequestMapping("/adminpanel")
    public String run(){
        return "admin";
    }

    /*@RequestMapping("/")
    public String runs(){
        return "index";
    }*/

    @RequestMapping("/")
    public ModelAndView login(@RequestParam Optional<String> error){
        return new ModelAndView("login", "error", error);
    }

    @RequestMapping("/api/private/getObjectId")
    @ResponseBody
    public long getObjectID() throws SQLException {
        return JDBCUtils.getInstance().generateId();
    }
}
