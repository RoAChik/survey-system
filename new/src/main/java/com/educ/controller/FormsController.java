package com.educ.controller;

import com.educ.model.generationForm.CompositeUIForm;
import com.educ.model.generationForm.UIForm;
import com.educ.model.generationForm.UISecureMap;
import com.educ.service.CurrentSecurityMap;
import com.educ.service.FormsModel;
import com.educ.service.JDBCUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Map;

@org.springframework.stereotype.Controller
@RestController
public class FormsController {

    FormsModel model = new FormsModel();

    @RequestMapping(value = "/api/private/createForm", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void setForm(@RequestBody CompositeUIForm generateForm) throws SQLException {
        model.add(generateForm);
    }

    @RequestMapping(value = "/api/private/updateForm", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void updateForm(@RequestBody CompositeUIForm generateForm) throws SQLException, IllegalAccessException {
        model.updateForm(generateForm);
    }

    @RequestMapping(value = "/api/public/getStudentFormForSurvey",method = RequestMethod.GET)
    public UIForm getStudentUIFormForSurvey(@RequestParam(value="id", defaultValue="1") String id) throws SQLException, IllegalAccessException {
        return model.getStudentUIForm(id, false);
    }

    @RequestMapping(value = "/api/public/getStudentFormForUpdate",method = RequestMethod.GET)
    public UIForm getStudentUIFormForUpdate(@RequestParam(value="id", defaultValue="1") String id) throws SQLException, IllegalAccessException {
        return model.getStudentUIForm(id, true);
    }

    @RequestMapping(value = "/api/private/getFormForModal",method = RequestMethod.GET)
    public UIForm getFormForModal(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        return model.getCuratorUIForm(id,false);
    }

    @RequestMapping(value = "/api/private/getGrants",method = RequestMethod.GET)
    public UISecureMap getGrants() throws SQLException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        UISecureMap uiSecureMap = new UISecureMap(securityMap.hasAdminGrants(),securityMap.hasAdminCentreGrants());
        return uiSecureMap;
    }

    @RequestMapping(value = "/api/private/getForms",method = RequestMethod.GET)
    public Map<Long, Map<String, String>> getForms(@RequestParam(value="id", defaultValue="1") String id) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        return jdbcUtils.getAllForms(id);
    }
}
