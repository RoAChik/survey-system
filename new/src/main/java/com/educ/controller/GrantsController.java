package com.educ.controller;

import com.educ.service.GrantsModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

/**
 * Created by anbu0216 on 30.05.2017.
 */
@RestController
public class GrantsController {

    GrantsModel model = new GrantsModel();

    @RequestMapping(value = "/api/private/setFieldGrants", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void setGrants(@RequestBody String request) throws SQLException {
        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(request).getAsJsonObject();
        Long field_id = o.get("id").getAsLong();
        JsonArray arr = o.get("grants").getAsJsonArray();
        for (JsonElement jsonElement : arr) {
            JsonObject ob = jsonElement.getAsJsonObject();
            Long role_id = ob.get("map").getAsJsonObject().get("role").getAsJsonObject().get("objectId").getAsLong();
            boolean read = ob.get("map").getAsJsonObject().get("read").getAsBoolean();
            boolean write = ob.get("map").getAsJsonObject().get("write").getAsBoolean();
            String readPart = (read==true)?"1":"0";
            String writePart = (write==true)?"1":"0";
            System.out.println("GRANT "+readPart+writePart);
            System.out.println("GRANT2 "+Integer.parseInt(writePart+readPart, 2));
            model.setGrants(field_id,role_id,Integer.parseInt(writePart+readPart, 2));
        }
    }
}
