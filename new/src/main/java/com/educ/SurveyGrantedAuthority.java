package com.educ;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by anbu0216 on 14.05.2017.
 */
public class SurveyGrantedAuthority implements GrantedAuthority {

    private String roleId;

    public SurveyGrantedAuthority(Long roleId){
        this.roleId = roleId.toString();

    }

    @Override
    public String getAuthority() {
        return this.roleId;
    }
}
