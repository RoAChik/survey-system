package com.educ.service;

import com.educ.model.EducObject;
import com.educ.model.User;
import com.educ.model.UserRole;
import com.educ.model.common.Response;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by anbu0216 on 24.04.2017.
 */
public class UserModel extends EducModel {

    protected static long OT = Constants.OT_USER;
    private static RolesModel rolesModel = new RolesModel();
    private static GrantsModel grantsModel = new GrantsModel();

    @Override
    protected long getOT() {
        return OT;
    }

    public User getObject(String id) throws SQLException {
        Map<String,ArrayList<String>> map = jdbcUtils.getObject(Long.valueOf(id));
        User object = new User();
        object.setObjectId(Long.parseLong(map.get("objectId").get(0)));
        object.setParentId(Long.parseLong(map.get("parent_id").get(0)));
        object.setName(map.get("name").get(0));
        ArrayList<UserRole> roles = new ArrayList<>();
        if(map.get("val_1007") != null) {
            ArrayList<String> rolesIds = map.get("val_1007");
            System.out.println("size = "+rolesIds.size());
            for (String rolesId : rolesIds) {
                roles.add(rolesModel.getObject(rolesId));
            }
        }
        if(map.get("val_1008") != null) {
            //String password = DigestUtils.md5Hex(map.get("val_1008").get(0)).toUpperCase();
            object.setPassword(map.get("val_1008").get(0));
        }
        object.setRoles(roles);
        //grantsModel.getGrants(obs);
        return object;
    }

    public User getObjectByName(String name) throws SQLException {
        Map<String,ArrayList<String>> map = jdbcUtils.getObjectByName(name,getOT());
        if(map.isEmpty()) return null;
        User object = new User();
        object.setObjectId(Long.parseLong(map.get("objectId").get(0)));
        object.setParentId(Long.parseLong(map.get("parent_id").get(0)));
        object.setName(map.get("name").get(0));
        ArrayList<UserRole> roles = new ArrayList<>();
        if(map.get("val_1007") != null) {
            ArrayList<String> rolesIds = map.get("val_1007");
            System.out.println("size = "+rolesIds.size());
            for (String rolesId : rolesIds) {
                roles.add(rolesModel.getObject(rolesId));
            }
        }
        if(map.get("val_1008") != null) {
            //String password = DigestUtils.md5Hex(map.get("val_1008").get(0)).toUpperCase();
            object.setPassword(map.get("val_1008").get(0));
        }
        object.setRoles(roles);
        return object;
    }


    public Response editObject(User object, Response response) throws SQLException {
        User check = getObjectByName(object.getName());
        if(check != null && check.getObjectId() != object.getObjectId()){
            response.setParam("error",true);
            response.setParam("errorMessage","The user exists");
        } else {
            jdbcUtils.updateObject(object.getObjectId(), object.getName());
            jdbcUtils.updateParam(object.getObjectId(), 1008, DigestUtils.md5Hex(object.getPassword()).toUpperCase());
            ArrayList<String> roles = new ArrayList<>();
            for (UserRole role : object.getRoles()) {
                roles.add(new String(role.getObjectId() + ""));
            }

            jdbcUtils.updateParams(object.getObjectId(), 1007, roles);
        }
        return response;
    }

    public Response addObject(EducObject object, Response response) throws SQLException {
        User check = getObjectByName(object.getName());
        if(check != null){
            response.setParam("error",true);
            response.setParam("errorMessage","The user exists");
        } else {
            addObject(object);
        }
        return response;
    }
}

