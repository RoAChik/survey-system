package com.educ.service;

import com.educ.model.EducObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by anbu0216 on 14.04.2017.
 */
public class EducModel {

    protected static long OT = 0L;
    JDBCUtils jdbcUtils = JDBCUtils.getInstance();

    public boolean addObject(EducObject object) throws SQLException {
        return jdbcUtils.insertObject(object.getObjectId(), object.getParentId(),object.getName(),getOT());
    }

    public boolean editObject(EducObject object) throws SQLException {
        System.out.println(object.getObjectId());
        return jdbcUtils.updateObject(object.getObjectId(), object.getName());
    }

    public Map<Long, Map<String,String>> getObjects() throws SQLException {
        return jdbcUtils.getAllObjects(getOT());
    }

    public EducObject getObject(String id) throws SQLException {
        Map<String,ArrayList<String>> map = jdbcUtils.getObject(Long.valueOf(id));
        EducObject object = new EducObject();
        object.setObjectId(Long.parseLong(map.get("objectId").get(0)));
        //object.setParentId(Long.parseLong(map.get("parent_id")));
        object.setName(map.get("name").get(0));
        return object;
    }

    public Map<Long, Map<String,String>> getObjectsByParent(String parent_id) throws SQLException {
        return jdbcUtils.getAllObjectsByParent(Long.valueOf(parent_id),getOT());
    }

    public Map<Long, Map<String,String>> getObjectsByParent(long parent_id) throws SQLException {
        return jdbcUtils.getAllObjectsByParent(parent_id,getOT());
    }

    protected long getOT(){
        return OT;
    }

}
