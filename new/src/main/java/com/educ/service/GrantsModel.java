package com.educ.service;

import com.educ.model.User;
import com.educ.model.UserRole;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.SQLException;
import java.util.*;

import static com.educ.service.Constants.ADMIN_ROLE;
import static com.educ.service.Constants.STUDENT_ROLE;

/**
 * Created by anbu0216 on 13.05.2017.
 */
public class GrantsModel {

    public static final int READ = 0;
    public static final int WRITE = 1;

    JDBCUtils jdbcUtils = JDBCUtils.getInstance();
    RolesModel rolesModel = new RolesModel();

    public HashMap<Long,Integer> getGrants(ArrayList<Long> objects) throws SQLException {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserRole> uRoles = user.getRoles();
        ArrayList<Long> roles = new ArrayList<>();
        for (UserRole uRole : uRoles) {
            roles.add(uRole.getObjectId());
        }
        HashMap<Long, Integer> grants = JDBCUtils.getInstance().getGrants(objects,roles);
        return grants;
    }

    public boolean check(int grants,int check){
        if(Utils.hasSecurityRole(ADMIN_ROLE)) return true;
        return checkClear(grants,check);
    }

    public boolean checkClear(int grants,int check){
        return ((grants >> check) & 1) == 1;
    }

    private HashMap<UserRole,Integer> getAllGrants(long fieldId, ArrayList<Long> roles) throws SQLException {
        LinkedHashMap<UserRole,Integer> map = new LinkedHashMap<>();

        HashMap<Long,Integer> grants = jdbcUtils.getGrantsForObject(fieldId,roles);
        for (Map.Entry<Long, Integer> longIntegerEntry : grants.entrySet()) {
            UserRole role = rolesModel.getObject(longIntegerEntry.getKey().toString());
            map.put(role,longIntegerEntry.getValue());
        }
        return map;
    }

    public HashMap<UserRole,Integer> getGrantsForField(Long fieldId, Long formId) throws SQLException {
        Map<String, ArrayList<String>> form = jdbcUtils.getObject(formId);
        Map<Long, Map<String,String>> roles = rolesModel.getObjectsByParent(form.get("parent_id").get(0));
        ArrayList<Long> rolesIds = new ArrayList<>();
        for (Long aLong : roles.keySet()) {
            rolesIds.add(aLong);
        }
        rolesIds.add(STUDENT_ROLE);
        return getAllGrants(fieldId,rolesIds);
    }

    public void setGrants(Long field_id, Long role_id, int grant) throws SQLException {
        jdbcUtils.insertGrants(field_id,role_id,grant);
    }
}
