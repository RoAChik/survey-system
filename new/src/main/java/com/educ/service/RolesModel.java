package com.educ.service;

import com.educ.model.UserRole;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by anbu0216 on 24.04.2017.
 */
public class RolesModel extends EducModel {

    protected static long OT = Constants.OT_USER_ROLE;

    @Override
    protected long getOT() {
        return OT;
    }

    public UserRole getObject(String id) throws SQLException {
        System.out.println("id = "+id);
        Map<String,ArrayList<String>> map = jdbcUtils.getObject(Long.valueOf(id));
        System.out.println(map);
        UserRole object = new UserRole();
        object.setObjectId(Long.parseLong(map.get("objectId").get(0)));
        object.setParentId(Long.parseLong(map.get("parent_id").get(0)));
        object.setName(map.get("name").get(0));
        return object;
    }

    public ArrayList<UserRole> getObjectsByIds(ArrayList<Long> ids) throws SQLException {
        Map<Long, Map<String, String>> map = jdbcUtils.getAllObjectsByIds(ids);

        ArrayList<UserRole> list = new ArrayList<>();
        for (Map<String, String> stringStringMap : map.values()) {
            UserRole object = new UserRole();
            object.setObjectId(Long.parseLong(stringStringMap.get("objectId")));
            object.setParentId(Long.parseLong(stringStringMap.get("parentId")));
            object.setName(stringStringMap.get("name"));
            list.add(object);
        }
        return list;
    }

}
