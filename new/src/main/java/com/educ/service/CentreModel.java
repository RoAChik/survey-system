package com.educ.service;

import com.educ.model.Centre;
import com.educ.model.User;
import com.educ.model.UserRole;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anbu0216 on 14.04.2017.
 */
public class CentreModel extends EducModel {

    protected static long OT = Constants.OT_CENTRE;
    RolesModel rolesModel = new RolesModel();

    @Override
    protected long getOT() {
        return OT;
    }

    public Centre getObject(String id) throws SQLException {
        Map<String,ArrayList<String>> map = jdbcUtils.getObject(Long.valueOf(id));
        Centre object = new Centre();
        object.setObjectId(Long.parseLong(map.get("objectId").get(0)));
        //object.setParentId(Long.parseLong(map.get("parent_id")));
        object.setName(map.get("name").get(0));
        return object;
    }


    @Override
    public Map<Long, Map<String, String>> getObjects() throws SQLException {
        Map<Long,Map<String,String>> list = super.getObjects();

        Map<Long,Map<String,String>> newList = new HashMap<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserRole> rolesList = user.getRoles();
        for (UserRole userRole : rolesList) {
            if(userRole.getObjectId() == Constants.ADMIN_ROLE){
                return list;
            }
        }
        for (Map.Entry<Long, Map<String, String>> longMapEntry : list.entrySet()) {
            if(user.getParentId() == longMapEntry.getKey()){
                newList.put(longMapEntry.getKey(),longMapEntry.getValue());
            }
        }
        return newList;
    }
}
