package com.educ.service;

import com.educ.model.Element;
import com.educ.model.generationForm.CompositeUIForm;
import com.educ.model.generationForm.FormField;
import com.educ.model.generationForm.FormOption;
import com.educ.model.generationForm.UIForm;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.educ.service.Constants.OBJECT_TYPES;
import static com.educ.service.Constants.OT_STUDENT_FORM;

/**
 * Created by anbu0216 on 18.05.2017.
 */
public class FormsModel {

    JDBCUtils jdbcUtils = JDBCUtils.getInstance();

    public void add(CompositeUIForm generateForm) throws SQLException {
        List<Element> form = new ArrayList<>();
        propagateElementsOnOneForm(generateForm.getStudentForm(), form, OT_STUDENT_FORM,generateForm.getParent_id());
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        jdbcUtils.insertForm(form);
    }

    public void updateForm(CompositeUIForm generateForm) throws SQLException, IllegalAccessException {
        List<Element> form = new ArrayList<>();
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        propagateElementsOnOneForm(generateForm.getStudentForm(), form, Constants.OT_STUDENT_FORM);
        jdbcUtils.updateStudentForm(form);
        form.clear();
    }

    public UIForm getCuratorUIForm(String id, Boolean isForUpdate) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<Element> form = null;
        ArrayList<Element> newList = new ArrayList<>();
        try {
            form = jdbcUtils.getCuratorFormByStudentQuestionnaireId(Long.parseLong(id), isForUpdate);
            GrantsModel gModel = new GrantsModel();
            ArrayList<Long> objects = new ArrayList<>();
            HashMap<Long,Integer> grants = gModel.getGrants(objects);

            /*for (Element element : form) {
                System.out.println("Check grants for "+element.getName()+" "+gModel.check(grants.get(element.getObjectId()),GrantsModel.WRITE));
                if(gModel.check(grants.get(element.getObjectId()),GrantsModel.READ)){
                    newList.add(element);
                }
            }*/
            //System.out.println(newList.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getUIStudentFormByFormElements(form,isForUpdate);
    }

    /*For student forms*/
    private UIForm getUIStudentFormByFormElements(List<Element> form, boolean isForUpdate) throws SQLException {
        return getUIFormByFormElements(form, Constants.OT_STUDENT_FORM, isForUpdate);
    }

    private void propagateElementsOnOneForm(UIForm form, List<Element> elements, Long objectType){
        propagateElementsOnOneForm(form,elements,objectType,null);
    }

    private void propagateElementsOnOneForm(UIForm form, List<Element> elements, Long objectType, Long parent_id){
        long formId = form.getId();
        long parentId = (parent_id == null)?formId:parent_id;
        Long[] parents = new Long[]{};
        elements.add(new Element(formId, parentId, objectType, form.getTitle(),
                null, false, parents, form.getOrder(), false));
        for (FormField formFields : form.getForm_fields()) {
            long elementId = formFields.getId();
            String elementName = formFields.getTitle();
            String elementTypeStr = formFields.getField_type();
            String elementDescription = formFields.getDescription();
            Long elementOrder = formFields.getOrder();
            Boolean isObsolete = formFields.getField_obsolete();
            String pseudonym = formFields.getPseudonym();

            isObsolete = isObsolete != null ? isObsolete : false;
            boolean elementIsRequired = formFields.getField_required();
            parents = new Long[]{formId};

            elements.add(new Element(elementId, formId, (Long) OBJECT_TYPES.get(elementTypeStr),
                    elementName, elementDescription, elementIsRequired,  parents, elementOrder, isObsolete,pseudonym));

            if (formFields.getField_options() != null) {
                for (FormOption formOptions : formFields.getField_options()) {
                    long childElementId = formOptions.getId();
                    String childElementName = formOptions.getTitle();
                    parents = new Long[]{elementId,formId};
                    Long optionOrder = formOptions.getOrder();
                    isObsolete = formOptions.getOption_obsolete();
                    isObsolete = isObsolete != null ? isObsolete : false;
                    elements.add(new Element(childElementId, elementId, Constants.OT_ELEMENT,
                            childElementName, null, false, parents, optionOrder, isObsolete));
                }
            }
        }
    }

    public UIForm getStudentUIForm(String id, Boolean isForUpdate) throws SQLException, IllegalAccessException {
        if(isForUpdate){
            CurrentSecurityMap currentSecurityMap = new CurrentSecurityMap();
            if(!currentSecurityMap.hasAdminGrants()) throw new IllegalAccessException();
        }
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<Element> form = null;
        try {
            form = jdbcUtils.getStudentFormById(Long.parseLong(id), isForUpdate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getUIStudentFormByFormElements(form,isForUpdate);
    }

    /*TODO: Put the method to the Form OT*/
    private UIForm getUIFormByFormElements(List<Element> form, long objectTypeId, boolean isForUpdate) throws SQLException {
        UIForm uiForm = new UIForm();
        GrantsModel gModel = new GrantsModel();
        ArrayList<Long> objects = new ArrayList<>();
        for(Element element: form) {
            objects.add(element.getObjectId());
        }

        HashMap<Long,Integer> grants = gModel.getGrants(objects);
        for(Element element: form){
            long OT = element.getOT();
            if(OT == objectTypeId){
                uiForm.setId(element.getObjectId());
                uiForm.setTitle(element.getName());
                List<FormField> formFields = new ArrayList<>();
                List<Element> fields = findChildren(element,form);
                for (Element field : fields) {
                    if(!gModel.check(grants.get(field.getObjectId()),GrantsModel.READ) && !isForUpdate){
                        continue;
                    }
                    FormField formField = new FormField();
                    formField.setId(field.getObjectId());
                    formField.setDescription(field.getDescription());
                    formField.setTitle(field.getName());
                    formField.setField_required(field.isRequired());
                    formField.setField_value(field.getValue());
                    formField.setField_type((String) Constants.OBJECT_TYPES.getKey(field.getOT()));
                    formField.setField_obsolete(field.getObsolete());
                    formField.setPseudonym(field.getPseudonym());
                    if(!gModel.check(grants.get(field.getObjectId()),GrantsModel.WRITE) && !isForUpdate){
                        formField.setField_disabled("true");
                    }
                    List<FormOption> formOptions = new ArrayList<>();
                    List<Element> options = findChildren(field,form);
                    for (Element option : options) {
                        FormOption formOption = new FormOption();
                        formOption.setId(option.getObjectId());
                        formOption.setTitle(option.getName());
                        formOption.setOption_value(option.getName());
                        boolean selected = (field.getValue()!=null && field.getValue().equals(option.getName()));
                        formOption.setOption_selected(selected);
                        formOption.setOption_obsolete(option.getObsolete());
                        formOptions.add(formOption);
                    }
                    formOptions.sort(new FieldComparator());
                    formField.setField_options(formOptions);
                    formFields.add(formField);

                }
                uiForm.setForm_fields(formFields);
            }
        }
        return uiForm;
    }

    private List<Element> findChildren(Element e, List<Element> form){
        List<Element> list = new ArrayList<>();

        for(int i = 0; i< form.size(); i++) {
            if(form.get(i).getParentId() == e.getObjectId() && form.get(i).getObjectId() != e.getObjectId()){
                list.add(form.get(i));
            }
        }
        return list;
    }
}
