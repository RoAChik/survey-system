package com.educ.model.generationForm;

/**
 * Created by anbu0216 on 02.12.2016.
 */
public class CompositeUIForm {
    private UIForm studentForm;
    private Long parent_id;

    public UIForm getStudentForm() {
        return studentForm;
    }

    public void setStudentForm(UIForm studentForm) {
        this.studentForm = studentForm;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }
}
