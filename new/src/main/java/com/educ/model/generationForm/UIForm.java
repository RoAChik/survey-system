package com.educ.model.generationForm;

import java.util.List;

public class UIForm extends UIElement {

    /*The field isn't null only for curator form*/
    private long queId;
    public UIForm(){}

    private List<FormField> form_fields;

    public List<FormField> getForm_fields() {
        return form_fields;
    }

    public void setForm_fields(List<FormField> form_fields) {
        this.form_fields = form_fields;
    }

    public long getQueId() {
        return queId;
    }

    public void setQueId(long queId) {
        this.queId = queId;
    }
}
