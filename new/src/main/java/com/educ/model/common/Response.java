package com.educ.model.common;

import java.util.HashMap;
import java.util.Map;

public class Response {
    Map<String,Object> params = new HashMap<>();

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public void setParam(String key, Object value){
        this.params.put(key,value);
    }
}
