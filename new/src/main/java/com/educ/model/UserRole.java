package com.educ.model;

/**
 * Created by anbu0216 on 13.04.2017.
 */
public class UserRole extends EducObject{

    private long centre;

    public long getCentre() {
        return centre;
    }

    public void setCentre(long centre) {
        this.centre = centre;
    }
}
