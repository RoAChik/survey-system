 DELETE FROM ids WHERE
	current_id > 0
	AND current_id <= 2001;
 INSERT INTO ids (current_id)
	values(10001);
	
 DELETE FROM object_types WHERE
	object_type_id > 0
	AND object_type_id < 1001;
 INSERT INTO object_types (object_type_id, name) values
	(1, 'form'),
	(8, 'element'),
	(9, 'questionnaire'),
	(10, 'attribute'),
	(11, 'curator_form'),
	(12, 'curator_questionnaire'),
	(13, 'hr_form'),
	(14, 'hr_questionnaire'),
	(15, 'manager_form'),
	(16, 'manager_questionnaire'),
	/*object_type_ids of new UI element should be between 100 and 200*/
	(100, 'text'),
	(101, 'checkbox'),
	(102, 'radio'),
	(103, 'select'),
	(104, 'date'),
	(105, 'textarea'),
	(106, 'phone_number'),
	(107, 'email'),
	(108, 'bounded_number'),
	(109, 'year_of_birth');
	/* ... */
 DELETE FROM objects WHERE
	object_id > 1000
	AND object_id < 2001;	

 INSERT INTO objects(object_id, parent_id, object_type_id, name) values
	(1002, 1002, 10, 'isRequired'),
	(1003, 1003, 10, 'isObsolete'),
	(1006, 1006, 10, 'order');

 INSERT INTO transition(object_id, parent_id, level) VALUES
	(1002, 1002, 0),
	(1003, 1003, 0),
	(1006, 1006, 0);

 CREATE INDEX objects_parent_id_index ON objects(parent_id);
 CREATE INDEX objects_obj_type_id_index ON objects(object_type_id);
 CREATE INDEX objects_obj_type_id_obj_id_index ON objects(object_id, object_type_id);
 CREATE INDEX params_obj_id_attr_id_index ON params(object_id, attr_id);
 CREATE INDEX transition_parent_id_obj_id_index ON transition(parent_id, object_id);