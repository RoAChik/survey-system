/*
* INPUT PARAMETERS:
* 1 - ORDER_ATTRIBUTE
* 2 - formId
* 3 - OBJECT_TYPE_CURATOR_FORM
*/
 SELECT o.object_id AS field_id,
	o.name AS field_name 
 FROM objects o, objects cur
	LEFT JOIN params p ON cur.object_id = p.object_id AND  
						p.attr_id = ?   
 WHERE cur.parent_id = ? AND
	cur.object_type_id = ? AND  
	o.parent_id = cur.object_id AND  
	o.object_type_id BETWEEN 100 AND 200
 ORDER BY cast(p.value as int) ASC