package com.educ.service;

import com.educ.model.Answer;
import com.educ.model.Element;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static com.educ.service.Constants.*;

public class JDBCUtils {
    private static Logger log = Logger.getLogger(JDBCUtils.class.getName());
    private String DB_CONNECTION = "jdbc:postgresql://localhost:5432/EDUC"; //port and name of database
    private final String DB_USER = "postgres";
    private final String DB_PASSWORD = "netcracker"; //password for user postgres, is set while isntalling
    private final String DB_DRIVER = "org.postgresql.Driver";

    private final String EDUC_DATABASE_CREATION_FILE_PATH = "/db/EDUC Schema_database_create.sql";
    private final String EDUC_TABLES_CREATION_FILE_PATH = "/db/EDUC Schema_tables_create.sql";
    private final String EDUC_DATAFIX_DIRECTORY_PATH = "/db/datafixes";
    private static final String EDUC_QUERIES_STORAGE_DIRECTORY_PATH = "/db/queries/";//"src/main/resources/db/queries/";

    private static String getStudentFormByIdQuery;
    private static String insertObjectQuery;

    private static final JDBCUtils instance = new JDBCUtils();

    static {
        try {
            getQueriesFromLocalStorage();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static JDBCUtils getInstance() {
        return instance;
    }

    private static void getQueriesFromLocalStorage() throws IOException {
        getStudentFormByIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getStudentFormByIdQuery.sql");
        insertObjectQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "insertObjectQuery.sql");
    }

    private Connection getConnection() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException ex) {
            log.log(Level.SEVERE, "Include PostgreSQL JDBC Driver in your library path! Exception: ", ex);
            return null;
        }

        log.log(Level.FINE, "PostgreSQL JDBC Driver Registered!");
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException ex) {
            log.log(Level.SEVERE, "Connection Failed! Check output console: ", ex);
            return null;
        }
        return connection;
    }

    private static Long currentId;
    private static Long lastAllottedId;

    public synchronized long generateId() throws SQLException {
        if (currentId == null || currentId > lastAllottedId) {
            currentId = JDBCUtils.getInstance().getIdAndSetId();
            lastAllottedId = currentId + 1000;
        }
        return currentId++;
    }

    /*IMPORTANT FOR DEVELOPMENT. DON'T DELETE*/
    public void createEmptyDataBaseForBackup() throws IOException, SQLException {
        DB_CONNECTION = "jdbc:postgresql://localhost:5432/";
        String sql = Utils.readAllLinesFromFile(EDUC_DATABASE_CREATION_FILE_PATH);
        executeSQLCode(sql);
    }

    /*IMPORTANT FOR DEVELOPMENT. DON'T DELETE*/
    public void createEmptyDataBase() throws IOException, SQLException {
        createEmptyDataBaseForBackup();
        DB_CONNECTION += "EDUC";
        String sql = Utils.readAllLinesFromFile(EDUC_TABLES_CREATION_FILE_PATH);
        executeSQLCode(sql);
        executeAllScriptsFromDirectory(EDUC_DATAFIX_DIRECTORY_PATH);
    }

    private void executeSQLCode(String sql) throws SQLException {
        Connection dbConnection = getConnection();
        dbConnection.setAutoCommit(true);
        Statement statement = dbConnection.createStatement();
        statement.execute(sql);
        if (statement != null) {
            statement.close();
        }
        dbConnection.close();
    }

    private void executeAllScriptsFromDirectory(String dirPath) throws SQLException, IOException {
        List<File> fileList;
        File directory = new ClassPathResource(dirPath).getFile();
        if (directory.listFiles() == null) {
            return;
        }
        fileList = new ArrayList<>(Arrays.asList(directory.listFiles()));
        for (File file : fileList) {
            if (!Pattern.matches("^.*\\.sql$", file.getName())) {
                fileList.remove(file);
            } else {
                String sql = Utils.readAllLinesFromFile(dirPath + "\\" + file.getName());
                executeSQLCode(sql);
            }
        }
    }

    public List<Element> getStudentFormById(long objectId, Boolean withObsolete) throws SQLException {
        Connection dbConnection = getConnection();
        PreparedStatement getStudentFormByIdPS = dbConnection.prepareStatement(getStudentFormByIdQuery);
        getStudentFormByIdPS.setLong(1, IS_REQUIRED_ATTRIBUTE);
        getStudentFormByIdPS.setLong(2, ORDER_ATTRIBUTE);
        getStudentFormByIdPS.setLong(3, IS_OBSOLETE_ATTRIBUTE);
        getStudentFormByIdPS.setLong(4, objectId);
        getStudentFormByIdPS.setLong(5, Constants.OT_CURATOR_FORM);
        getStudentFormByIdPS.setLong(6, Constants.OT_HR_FORM);
        getStudentFormByIdPS.setLong(7, Constants.OT_MANAGER_FORM);
        return getFormByStatment(getStudentFormByIdPS, withObsolete);
    }


    private List<Element> getFormByStatment(PreparedStatement getFormPS, Boolean withObsolete) throws SQLException {
        List<Element> elements = new ArrayList<>();
        System.out.println(getFormPS.toString());
        ResultSet rs = getFormPS.executeQuery();
        while (rs.next()) {
            Element element = new Element(rs.getLong("object_id"), rs.getLong("parent_id"), rs.getLong("object_type_id"),
                    rs.getString("name"), rs.getString("description"), rs.getBoolean("is_required"),
                    rs.getInt("fields_order"), rs.getBoolean("is_obsolete"));
            if (rs.getString("field_value") != null) {
                element.setValue(rs.getString("field_value"));
            }
            if (!element.getObsolete() || withObsolete) {
                elements.add(element);
            }
        }
        getFormPS.getConnection().close();
        return elements;
    }

    private void insertQueElement(Element element, Connection dbConnectionWithAutoCommitOff) throws SQLException {
        if (element == null) {
            return;
        }
        StringBuilder transitionsInsertSql = new StringBuilder("INSERT INTO transition " +
                "(object_id, parent_id, level) VALUES ");
        List<Long> parentsIds = element.getParents();
        transitionsInsertSql.append("\n(" +
                element.getObjectId() + ", " +
                element.getObjectId() + ", " +
                0 + "),");
        if (parentsIds != null) {
            for (int j = 0; j < parentsIds.size(); j++) {
                transitionsInsertSql.append("\n(" +
                        element.getObjectId() + ", " +
                        parentsIds.get(j) + ", " +
                        (j + 1) + "),");
            }
        }
        transitionsInsertSql.deleteCharAt(transitionsInsertSql.length() - 1);

        StringBuilder paramsInsertSql = new StringBuilder("INSERT INTO params " +
                "(attr_id, object_id, value) VALUES ");
        int paramInsertCounter = 0;
        if (element.isRequired()) {
            paramsInsertSql.append("\n(" +
                    IS_REQUIRED_ATTRIBUTE + ", " +
                    element.getObjectId() + ",'" +
                    "true'),");
            paramInsertCounter++;
        }

        Long order = element.getOrder();
        if (order != null) {
            paramsInsertSql.append("\n(" +
                    ORDER_ATTRIBUTE + ", " +
                    element.getObjectId() + ",'" +
                    order + "'),");
            paramInsertCounter++;
        }
        paramsInsertSql.deleteCharAt(paramsInsertSql.length() - 1);

        PreparedStatement insertObjectPreparedStatement = dbConnectionWithAutoCommitOff.prepareStatement(insertObjectQuery);
        insertObjectPreparedStatement.setLong(1, element.getObjectId());
        insertObjectPreparedStatement.setLong(2, element.getOT());
        insertObjectPreparedStatement.setLong(3, element.getParentId());
        insertObjectPreparedStatement.setString(4, element.getName());
        insertObjectPreparedStatement.setString(5, element.getDescription());
        insertObjectPreparedStatement.execute();

        dbConnectionWithAutoCommitOff.createStatement().execute(transitionsInsertSql.toString());
        if (paramInsertCounter > 0) {
            dbConnectionWithAutoCommitOff.createStatement().execute(paramsInsertSql.toString());
        }

    }


    public void insertStudentQuestionnaire(List<Answer> answers, Long parentId) throws SQLException {
        insertQuestionnaire(answers, parentId, OT_STUDENT_QUE, false,  null);
    }

    private boolean checkQueIsValid(List<Answer> answers, Long parentId, long questionnaireOT) throws SQLException {
        List<Element> form = getStudentFormById(parentId, false);
        Utils utils = new Utils();
        Boolean queIsValid = true;
        int questionCounter = 0;

        if (form == null){
            throw new RuntimeException("Form wasn't found!");
        }
        ArrayList<Long> objects = new ArrayList<>();
        for(Element element: form) {
            objects.add(element.getObjectId());
        }

        GrantsModel gModel = new GrantsModel();
        HashMap<Long,Integer> grants = gModel.getGrants(objects);
        for (Element formField : form) {
            if (!queIsValid) { break; }

            if (formField.getOT() < 100 || formField.getOT() > 200 || formField.getObsolete()) {
                continue;
            }

            if(!gModel.check(grants.get(formField.getObjectId()),GrantsModel.READ)){
                continue;
            }
            Answer answer = formField.findRelatedAnswer(answers);
            if (answer == null || answer.isEmpty()) {
                queIsValid = !formField.isRequired();
                questionCounter++;
                continue;
            }

            String pattern = null;
            switch ((int) formField.getOT()) {
                case (int) OT_EMAIL:
                    pattern = EMAIL_PATTERN;
                    break;
                case (int) OT_BOUNDED_NUMBER:
                    pattern = NUMBER_PATTERN;
                    break;
                case (int) OT_YEAR_OF_BIRTH:
                    pattern = NUMBER_PATTERN;
                    break;
                case (int) OT_DATE:
                    answer.editValue(answer.getValue().replace("T21:00:00.000Z", ""));
                    pattern = DATE_PATTERN;
                    break;
                case (int) OT_PHONE_NUMBER:
                    pattern = PHONE_PATTERN;
                    break;
            }
            /*For all other fields there is no regexp*/
            queIsValid = pattern == null || answer.getValue().matches(pattern);

            if (formField.getOT() == OT_YEAR_OF_BIRTH) {
                queIsValid = utils.tryParseInt(answer.getValue());
                if (queIsValid) {
                    Integer yearOfBirth = Integer.parseInt(answer.getValue());
                    queIsValid = (MIN_DATE_OF_BIRTH <= yearOfBirth && yearOfBirth <= MAX_DATE_OF_BIRTH);
                }
            }
            questionCounter++;
        }
        /*User shouldn't send more answers then form contains*/
        Set<Long> distinctAnswerIds = new HashSet<>();
        for (Answer answer: answers){
            distinctAnswerIds.add(answer.getFieldId());
        }
        System.out.println(questionCounter+" "+distinctAnswerIds.size());
        if (questionCounter != distinctAnswerIds.size()){
            queIsValid = false;
        }

        return queIsValid;
    }


    private void insertQuestionnaire(List<Answer> answers, Long formId, long questionnaireOT, boolean withoutCheck, Long queId) throws SQLException {
        if ((answers != null) && (answers.size() != 0)) {
            long questionnaireId = answers.get(0).getQuestionnaireId();
            if (!withoutCheck && !checkQueIsValid(answers, formId, questionnaireOT)) {
                throw new IllegalArgumentException("One of the fields filled incorrect");
            }

            Connection dbConnection = getConnection();
            dbConnection.setAutoCommit(false);
                insertQueElement(new Element(questionnaireId, formId, questionnaireOT, String.valueOf(questionnaireId)), dbConnection);
                StringBuilder answersInsertQuery = new StringBuilder("INSERT INTO params (attr_id, object_id, value) VALUES ");
                for (Answer answer : answers) {
                    String value = answer.getValue() != null ? answer.getValue() : "";
                    value = StringEscapeUtils.escapeSql(value);
                    answersInsertQuery.append("\n(" + answer.getFieldId() + ", " + questionnaireId +
                            ",'" + value + "'),");
                }
                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                Date date = new Date();
                answersInsertQuery.append("\n(" + SUBMIT_DATE + ", " + questionnaireId +
                    ",'" + dateFormat.format(date) + "'),");
                answersInsertQuery.deleteCharAt(answersInsertQuery.length() - 1);

                dbConnection.createStatement().execute(answersInsertQuery.toString());
            dbConnection.commit();
            dbConnection.close();
        }
    }


    private long getIdAndSetId() throws SQLException {
        long lastId = getLastIdFromTable("ids");
        PreparedStatement setIdPS = getConnection().prepareStatement("INSERT INTO ids (current_id) VALUES ( ? )");
        setIdPS.setLong(1, lastId + 1001);
        setIdPS.execute();
        setIdPS.getConnection().close();
        return ++lastId;
    }

    private long getLastIdFromTable(String table) throws SQLException {
        long lastId = 0;
        table = table.toUpperCase();

        String tableId = Constants.TABLE_2_ID.get(table);
        if (tableId == null) {
            throw new IllegalArgumentException("Unknown table");
        }
        PreparedStatement getLastIdPS = getConnection().
                prepareStatement("SELECT " + tableId + " FROM " + table + " ORDER BY " + tableId + " DESC LIMIT 1");

        ResultSet rs = getLastIdPS.executeQuery();
        if (rs.next()) {
            long lastIndex = rs.getLong(tableId);
            lastId = lastIndex + 1;
        }
        getLastIdPS.getConnection().close();
        return lastId;
    }


    private Map<String, ArrayList<String>> getParams(long id) throws SQLException {
        Map<String, ArrayList<String>> formMap = new HashMap<>();
        Connection dbConnection = getConnection();
        PreparedStatement getObjectPS = dbConnection.prepareStatement("select * from params where object_id = ?");
        getObjectPS.setLong(1, id);
        ResultSet rs = getObjectPS.executeQuery();
        // Multiple doesn't support... Yet...
        while (rs.next()) {
            putMap(formMap, "val_" + String.valueOf(rs.getLong("attr_id")), rs.getString("value"));
            //formMap.put("val_"+String.valueOf(rs.getLong("attr_id")), rs.getString("value"));
        }
        dbConnection.close();
        return formMap;
    }


    private void putMap(Map<String, ArrayList<String>> map, String attr, String value) {
        if (map.get(attr) == null) {
            ArrayList<String> list = new ArrayList<>();
            list.add(value);
            map.put(attr, list);
        } else {
            map.get(attr).add(value);
        }
    }


    public HashMap<Long, Integer> getGrants(ArrayList<Long> objects, ArrayList<Long> roles) throws SQLException {

        HashMap<Long, Integer> map = new HashMap<>();
        for (Long object : objects) {
            map.put(object, 0);
        }
        String sql = "SELECT * from grants where object_id = ANY(?) and role_id = ANY(?) order by grants asc";
        Connection dbConnection = getConnection();
        PreparedStatement getObjectPS = dbConnection.prepareStatement(sql);
        Array obj_ids = dbConnection.createArrayOf("bigint", objects.toArray());
        Array roles_ids = dbConnection.createArrayOf("bigint", roles.toArray());

        getObjectPS.setArray(1, obj_ids);
        getObjectPS.setArray(2, roles_ids);
        ResultSet rs = getObjectPS.executeQuery();
        while (rs.next()) {
            map.put(rs.getLong("object_id"), rs.getInt("grants"));
        }
        return map;
    }
}
