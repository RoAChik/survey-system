package com.educ.service;

import com.educ.model.User;
import com.educ.model.UserRole;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.SQLException;
import java.util.*;

import static com.educ.service.Constants.ADMIN_ROLE;

/**
 * Created by anbu0216 on 13.05.2017.
 */
public class GrantsModel {

    public static final int READ = 0;
    public static final int WRITE = 1;

    public HashMap<Long,Integer> getGrants(ArrayList<Long> objects) throws SQLException {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserRole> uRoles = user.getRoles();
        ArrayList<Long> roles = new ArrayList<>();
        for (UserRole uRole : uRoles) {
            roles.add(uRole.getObjectId());
        }
        HashMap<Long, Integer> grants = JDBCUtils.getInstance().getGrants(objects,roles);
        return grants;
    }

    public boolean check(int grants,int check){
        if(Utils.hasSecurityRole(ADMIN_ROLE)) return true;
        return ((grants >> check) & 1) == 1;
    }

}
