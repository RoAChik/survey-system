package com.educ.service;

import com.educ.model.Element;
import com.educ.model.User;
import com.educ.model.UserRole;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static String readAllLinesFromFile(String fileName) throws IOException {
        InputStream fileInputStream = new ClassPathResource(fileName).getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
		reader.close();
        return out.toString();
    }
    
    public List<Element> findChildren(Element e, List<Element> form){
        List<Element> list = new ArrayList<>();
        for (Element element : form) {
            if (element.getParentId() == e.getObjectId() && element.getObjectId() != e.getObjectId()) {
                list.add(element);
            }
        }
        return list;
    }

    public static boolean hasSecurityRole(Long role) {
        boolean has = false;
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserRole> rolesList = principal.getRoles();
        for (UserRole userRole : rolesList) {
            if(userRole.getObjectId() == role){
                has = true;
            }
        }

        return has;
    }

    public boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    public boolean tryParseLong(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }
}