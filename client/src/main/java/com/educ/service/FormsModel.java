package com.educ.service;

import com.educ.model.Element;
import com.educ.model.generationForm.FormField;
import com.educ.model.generationForm.FormOption;
import com.educ.model.generationForm.UIForm;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by anbu0216 on 18.05.2017.
 */
public class FormsModel {


    /*For student forms*/
    private UIForm getUIStudentFormByFormElements(List<Element> form, boolean isForUpdate) throws SQLException {
        return getUIFormByFormElements(form, Constants.OT_STUDENT_FORM, isForUpdate);
    }



    public UIForm getStudentUIForm(String id, Boolean isForUpdate) throws SQLException, IllegalAccessException {
        if(isForUpdate){
            CurrentSecurityMap currentSecurityMap = new CurrentSecurityMap();
            if(!currentSecurityMap.hasAdminGrants()) throw new IllegalAccessException();
        }
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<Element> form = null;
        try {
            form = jdbcUtils.getStudentFormById(Long.parseLong(id), isForUpdate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getUIStudentFormByFormElements(form,isForUpdate);
    }

    /*TODO: Put the method to the Form OT*/
    private UIForm getUIFormByFormElements(List<Element> form, long objectTypeId, boolean isForUpdate) throws SQLException {
        UIForm uiForm = new UIForm();
        GrantsModel gModel = new GrantsModel();
        ArrayList<Long> objects = new ArrayList<>();
        for(Element element: form) {
            objects.add(element.getObjectId());
        }

        HashMap<Long,Integer> grants = gModel.getGrants(objects);
        for(Element element: form){
            long OT = element.getOT();
            if(OT == objectTypeId){
                uiForm.setId(element.getObjectId());
                uiForm.setTitle(element.getName());
                List<FormField> formFields = new ArrayList<>();
                List<Element> fields = findChildren(element,form);
                for (Element field : fields) {
                    if(!gModel.check(grants.get(field.getObjectId()),GrantsModel.READ) && !isForUpdate){
                        System.out.println("ASD" + field.getName());
                        continue;
                    }
                    FormField formField = new FormField();
                    formField.setId(field.getObjectId());
                    formField.setDescription(field.getDescription());
                    formField.setTitle(field.getName());
                    formField.setField_required(field.isRequired());
                    formField.setField_value(field.getValue());
                    formField.setField_type((String) Constants.OBJECT_TYPES.getKey(field.getOT()));
                    formField.setField_obsolete(field.getObsolete());
                    if(!gModel.check(grants.get(field.getObjectId()),GrantsModel.WRITE) && !isForUpdate){
                        formField.setField_disabled("true");
                    }
                    List<FormOption> formOptions = new ArrayList<>();
                    List<Element> options = findChildren(field,form);
                    for (Element option : options) {
                        FormOption formOption = new FormOption();
                        formOption.setId(option.getObjectId());
                        formOption.setTitle(option.getName());
                        formOption.setOption_value(option.getName());
                        boolean selected = (field.getValue()!=null && field.getValue().equals(option.getName()));
                        formOption.setOption_selected(selected);
                        formOption.setOption_obsolete(option.getObsolete());
                        formOptions.add(formOption);
                    }
                    formOptions.sort(new FieldComparator());
                    formField.setField_options(formOptions);
                    formFields.add(formField);

                }
                uiForm.setForm_fields(formFields);
            }
        }
        return uiForm;
    }

    private List<Element> findChildren(Element e, List<Element> form){
        List<Element> list = new ArrayList<>();

        for(int i = 0; i< form.size(); i++) {
            if(form.get(i).getParentId() == e.getObjectId() && form.get(i).getObjectId() != e.getObjectId()){
                list.add(form.get(i));
            }
        }
        return list;
    }
}
