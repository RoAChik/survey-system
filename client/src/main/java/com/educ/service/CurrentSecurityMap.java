package com.educ.service;

import java.sql.SQLException;

/**
 * Created by GriGri on 17.01.2017.
 */
public class CurrentSecurityMap {
    private final Boolean hasAdminGrants;
    private final Boolean hasAdminCentreGrants;


    public CurrentSecurityMap() throws SQLException {
        hasAdminGrants = Utils.hasSecurityRole(Constants.ADMIN_ROLE);
        hasAdminCentreGrants = Utils.hasSecurityRole(Constants.ADMIN_CENTRE_ROLE);
    }

    public Boolean hasAdminGrants() {
        return hasAdminGrants;
    }

    public Boolean hasAdminCentreGrants() {
        return hasAdminCentreGrants;
    }
}
