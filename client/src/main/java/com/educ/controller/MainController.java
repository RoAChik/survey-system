package com.educ.controller;

import com.educ.model.User;
import com.educ.model.UserRole;
import com.educ.service.JDBCUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;
import java.util.ArrayList;

import static com.educ.service.Constants.STUDENT_ROLE;

@org.springframework.stereotype.Controller

public class MainController {


    @RequestMapping("/")
    public String run(){
        User user = new User();
        user.setRoles(new ArrayList(){{
            add(new UserRole(){{
                setObjectId(STUDENT_ROLE);
            }});
        }});
        Authentication auth =
                new UsernamePasswordAuthenticationToken(user, null, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "index";
    }


    @RequestMapping("/api/private/getObjectId")
    @ResponseBody
    public long getObjectID() throws SQLException {
        return JDBCUtils.getInstance().generateId();
    }
}
