package com.educ.controller;

import com.educ.model.Answer;
import com.educ.model.generationForm.FormField;
import com.educ.model.generationForm.FormOption;
import com.educ.model.generationForm.UIForm;
import com.educ.service.JDBCUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RestController
public class QuestionnairesController {

    @RequestMapping(value = "/api/public/submitQuestionnaire", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void submitQuestionnaire(@RequestBody UIForm uiFormWithAnswers,
                                    HttpServletResponse response) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        long questionnaireId = jdbcUtils.generateId();
        List<Answer> answers = new ArrayList<>();
        for (FormField formField : uiFormWithAnswers.getForm_fields()){
            if (formField.getField_type() == null) continue;
            Answer answer;
            String value = formField.getField_value();
            Boolean isRequired = formField.getField_required();
            long fieldId = formField.getId();
            if (formField.getField_options() != null && formField.getField_options().size() !=0){
                boolean added = false;
                for(FormOption formOption : formField.getField_options()){
                    String optionValue = formOption.getTitle();
                    if(formOption.getOption_selected() == true){
                        added = true;
                        answer = new Answer(questionnaireId, fieldId, optionValue, isRequired);
                        answers.add(answer);
                    }
                }
                if(!added){
                    answer = new Answer(questionnaireId, fieldId, "", isRequired);
                    answers.add(answer);
                }
            }
            else {
                answer = new Answer(questionnaireId, fieldId, value, isRequired);
                answers.add(answer);
            }
        }
        jdbcUtils.insertStudentQuestionnaire(answers, uiFormWithAnswers.getId());
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
