package com.educ.controller;

import com.educ.model.generationForm.UIForm;
import com.educ.service.FormsModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@org.springframework.stereotype.Controller
@RestController
public class FormsController {

    FormsModel model = new FormsModel();

    @RequestMapping(value = "/api/public/getStudentFormForSurvey",method = RequestMethod.GET)
    public UIForm getStudentUIFormForSurvey(@RequestParam(value="id", defaultValue="1") String id) throws SQLException, IllegalAccessException {
        return model.getStudentUIForm(id, false);
    }

}
