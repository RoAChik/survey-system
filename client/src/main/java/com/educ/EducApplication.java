package com.educ;

import com.educ.service.JDBCUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.sql.SQLException;

@SpringBootApplication
public class EducApplication {

	public static void main(String[] args) throws SQLException, IOException {
		JDBCUtils jdbcUtils = JDBCUtils.getInstance();
		//jdbcUtils.createEmptyDataBase();

		//Map<Long, Map<Long, ExtValue>> getQuestionnairesByFormId = jdbcUtils.getQuestionnairesByFormId(2015);
		//System.out.print(getQuestionnairesByFormId.toString());
		SpringApplication.run(EducApplication.class, args);


	}


}
