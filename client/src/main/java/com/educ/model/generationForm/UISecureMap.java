package com.educ.model.generationForm;

public class UISecureMap {
    private Boolean hasAdminGrants;
    private Boolean hasAdminCentreGrants;

    public UISecureMap(Boolean hasAdminGrants,Boolean hasAdminCentreGrants){
        this.hasAdminGrants = hasAdminGrants;
        this.hasAdminCentreGrants = hasAdminCentreGrants;
    }

    public Boolean getHasAdminCentreGrants() {
        return hasAdminCentreGrants;
    }

    public void setHasAdminCentreGrants(Boolean hasAdminCentreGrants) {
        this.hasAdminCentreGrants = hasAdminCentreGrants;
    }

    public Boolean getHasAdminGrants() {
        return hasAdminGrants;
    }

    public void setHasAdminGrants(Boolean hasAdminGrants) {
        this.hasAdminGrants = hasAdminGrants;
    }
}
