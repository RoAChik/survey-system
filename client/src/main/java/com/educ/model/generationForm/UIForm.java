package com.educ.model.generationForm;

import java.util.List;

public class UIForm extends UIElement {

    public UIForm(){}

    private List<FormField> form_fields;

    public List<FormField> getForm_fields() {
        return form_fields;
    }

    public void setForm_fields(List<FormField> form_fields) {
        this.form_fields = form_fields;
    }

}
