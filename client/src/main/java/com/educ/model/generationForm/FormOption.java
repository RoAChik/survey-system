package com.educ.model.generationForm;

/**
 * Created by INDIGO-ПС on 07.11.2016.
 */
public class FormOption  extends UIElement {

    private String option_value;
    private Boolean option_selected;
    private Boolean option_obsolete;

    public String getOption_value() {
        return option_value;
    }

    public void setOption_value(String option_value) {
        this.option_value = option_value;
    }

    public Boolean getOption_selected() {
        return option_selected;
    }

    public void setOption_selected(Boolean option_selected) {
        this.option_selected = option_selected;
    }

    public Boolean getOption_obsolete() {
        return option_obsolete;
    }

    public void setOption_obsolete(Boolean option_obsolete) {
        this.option_obsolete = option_obsolete;
    }
}
