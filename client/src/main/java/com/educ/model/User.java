package com.educ.model;

import java.util.List;

/**
 * Created by anbu0216 on 13.04.2017.
 */
public class User extends EducObject{

    private List<UserRole> roles;

    private String password;

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
