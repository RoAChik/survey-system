package com.educ.model;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by grpa0216 on 17.11.2016.
 */
public class Answer {
    long fieldId;
    String value;
    long questionnaireId;
    Boolean isRequired;

    public Answer(long questionnaireId, long fieldId, String value, Boolean isRequired){
        this.questionnaireId = questionnaireId;
        this.fieldId = fieldId;
        this.value = value;
        this.isRequired = isRequired;
    }

    public void editValue(String value) {
        this.value = value;
    }

    public long getFieldId(){
        return fieldId;
    }

    public long getQuestionnaireId() {
        return questionnaireId;
    }

    public String getValue() {
        return value;
    }

    public Boolean isRequired() {
        return isRequired;
    }

    public boolean isEmpty(){
        return value == null || value.isEmpty();
    }
}
