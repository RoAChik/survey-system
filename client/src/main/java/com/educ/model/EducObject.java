package com.educ.model;

/**
 * Created by anbu0216 on 24.04.2017.
 */
public class EducObject {

        private long objectId;

        private long parentId;

        private String name;

        public long getObjectId() {
            return objectId;
        }

        public void setObjectId(long objectId) {
            this.objectId = objectId;
        }

        public long getParentId() {
        return parentId;
        }

        public void setParentId(long parentId) {
            this.parentId = parentId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
}
