package com.educ.model;

import java.util.Arrays;
import java.util.List;

/**
 * Created by GriGri on 26.10.2016.
 */
public class Element {
    long objectId;
    long parentId;
    long OT;
    String name;
    String description;
    Boolean isRequired = false;
    Long order;
    String value;
    Boolean obsolete = false;
    List<Long> parents; //Only for parents!!!


    public Element(long objectId, long parentId, long OT, String name){
        this.objectId = objectId;
        this.parentId = parentId;
        this.OT = OT;
        this.name = name;
        this.description = null;
        this.isRequired = false;
    }

    public Element(long objectId, long parentId, long OT, String name, String description, boolean isRequired, Long[] parents, Long order, boolean isObsolete){
        this.objectId = objectId;
        this.parentId = parentId;
        this.OT = OT;
        this.name = name;
        this.description = description;
        this.isRequired = isRequired;
        this.order = order;
        this.setParents(parents);
        this.obsolete = isObsolete;
    }

    public Element(long objectId, long parentId, long OT, String name, String description, boolean isRequired, long order, boolean isObsolete){
        this.objectId = objectId;
        this.parentId = parentId;
        this.OT = OT;
        this.name = name;
        this.description = description;
        this.isRequired = isRequired;
        this.order = order;
        this.obsolete = isObsolete;
    }

    public Answer findRelatedAnswer(List<Answer> answers){
        for(Answer answer: answers){
            if (getObjectId() == answer.getFieldId()){
                return answer;
            }
        }
        return null;
    }


    public long getObjectId() {
        return objectId;
    }

    public long getOT() {
        return OT;
    }

    public long getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public boolean isRequired() {return isRequired;}

    public List<Long> getParents() {
        return parents;
    }

    public void setParents(List<Long> parents) {
        this.parents = parents;
    }

    public void setParents(Long[] parents) {
        this.parents = Arrays.asList(parents);
    }

    public Long getOrder() {
        return order;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getObsolete() {
        return obsolete;
    }

    public void setObsolete(Boolean obsolete) {
        obsolete = obsolete;
    }
}


    /*public String getParam(long attrId) throws SQLException {
        return JDBCUtils.getInstance().getParam(this.getObjectId(), attrId);
    }

    public void setParam(long attrId, String value) throws SQLException {
        JDBCUtils.getInstance().setParam(this.getObjectId(), attrId, value);
    };*/
